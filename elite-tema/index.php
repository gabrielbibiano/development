<?php include_once 'functions.php'; ?>

<section id="container">
	<?php add_action( 'wp_head', 'add_default_favicon' ); ?>

	<?php get_header(); ?>

	<section id="content">

	<?php include_once "banner.php" ?>

	<?php include_once 'principal.php'; ?>


	<?php get_footer(); ?>

	</section>
</section>