
<section id="container">
<?php get_header(); ?>
	<div class="content">
		<div class="main-page">
		<?php include_once "banner.php" ?>

				<?php
				while ( have_posts() ) : the_post();
					the_content();
					the_post_thumbnail();
				endwhile;
				?>

		</div>

		</div>
	<br><br><br><br>

<?php get_footer(); ?>

</section>

