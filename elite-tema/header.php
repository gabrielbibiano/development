<!DOCTYPE html>
<html>
<head>
	<title>Elite Imóveis</title>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="content-type" content="<?php bloginfo('html_type');?> <?php bloginfo('charset');?>">
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url');?>" media="all">
	<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1, maximum-scale=1, user-scalable=no" />	


	<?php wp_head(); ?>
</head>
<body>
<header>
	<?php if ( get_header_image() ) : ?>
	<div id="site-header">
	
		<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
			<img src="<?php header_image(); ?>" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
		</a>
	</div>
	<?php endif; ?>
		<input type="checkbox" id="control-nav" />
	    <label for="control-nav" class="control-nav"></label>
	    <label for="control-nav" class="control-nav-close"></label>
	<nav class="fR">

		<ul class="l2">
			<?php wp_nav_menu('title_li'); ?>
		</ul>
	</nav>






</header>
