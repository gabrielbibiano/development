<?php 
/*WIDGETS*/


	add_theme_support( 'post-thumbnails' ); 

	add_custom_background();

	add_filter('mce_css', 'my_editor_style');
	function my_editor_style($url) {
	  if ( !empty($url) )
	    $url .= ',';
	  // Change the path here if using sub-directory
	  $url .= trailingslashit( get_stylesheet_directory_uri() ) . 'editor-style.css';

	  return $url;
	}

	register_nav_menus( array(
		'primary' => __( 'Primary Menu',  'Elite' ),
	) );

 ?>